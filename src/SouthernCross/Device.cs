﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SouthernCross
{
    class Device
    {
        // Setup the private version of all the properties
        //-------------------------------------------------
        private String m_uuid = "";
        private double m_latitude = 0;
        private double m_longitude = 0;
        private long m_timestamp = 0;

        // Constructor for bare minimum instance
        //----------------------------------------
        public Device()
        {
        }

        // Constructor for fully populated instance
        //-------------------------------------------
        public Device(String uuid, double latitude, double longitude, long timestamp)
        {
            // Make sure the public getters and setters
            // are used so that the vallidation is envoked
            //----------------------------------------------
            this.uuid = uuid;
            this.latitude = latitude;
            this.longitude = longitude;
            this.timestamp = timestamp;
        }

        // Declare the public uuid property
        //-----------------------------------
        public string uuid
        {
            get { return this.m_uuid; }
            set { this.m_uuid = value; }
        }

        // Declare the public latitude property
        // Range: 0 to (+/-)90
        //---------------------------------------
        public double latitude
        {
            get { return this.m_latitude; }
            set 
            {
                // Check the data range first
                //-------------------------------
                if (value < -90 || value > 90)
                {
                    throw new System.ArgumentOutOfRangeException(
                        "latitude", 
                        "Value must be between -90.0 and 90.0, but " + value + " was specified.");
                }
                else
                {
                    // Store the validated data
                    //--------------------------
                    this.m_latitude = value;
                }
            }
        }

        // Declare the public longitude property
        // Range: 0 to (+/-)180
        //----------------------------------------
        public double longitude
        {
            get { return m_longitude; }
            set
            {
                // Check the data range first
                //-------------------------------
                if (value < -180.0 || value > 180.0)
                {
                    throw new System.ArgumentOutOfRangeException(
                        "longitude", 
                        "Value must be between -180.0 and 180.0, but " + value + " was specified.");
                }
                else
                {
                    // Store the validated data
                    //--------------------------
                    this.m_longitude = value;
                }
            }
        }

        // Declare the public timestamp property
        //-----------------------------------
        public long timestamp
        {
            get { return this.m_timestamp; }
            set { this.m_timestamp = value; }
        }
    }
}
