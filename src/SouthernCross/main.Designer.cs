﻿namespace SouthernCross
{
    partial class main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ProcessData = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.UniqueCount = new System.Windows.Forms.TextBox();
            this.TotalCount = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.DeviceCount = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.OldestUUID = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.GeoCount = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.NewestUUID = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.OldestDate = new System.Windows.Forms.TextBox();
            this.NewestDate = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // ProcessData
            // 
            this.ProcessData.Location = new System.Drawing.Point(467, 133);
            this.ProcessData.Name = "ProcessData";
            this.ProcessData.Size = new System.Drawing.Size(125, 23);
            this.ProcessData.TabIndex = 0;
            this.ProcessData.Text = "Process Data";
            this.ProcessData.UseVisualStyleBackColor = true;
            this.ProcessData.Click += new System.EventHandler(this.ProcessData_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 43);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(75, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Unique Count:";
            // 
            // UniqueCount
            // 
            this.UniqueCount.Location = new System.Drawing.Point(88, 40);
            this.UniqueCount.Name = "UniqueCount";
            this.UniqueCount.Size = new System.Drawing.Size(61, 20);
            this.UniqueCount.TabIndex = 3;
            // 
            // TotalCount
            // 
            this.TotalCount.Location = new System.Drawing.Point(88, 12);
            this.TotalCount.Name = "TotalCount";
            this.TotalCount.Size = new System.Drawing.Size(61, 20);
            this.TotalCount.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(21, 15);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Total Count:";
            // 
            // DeviceCount
            // 
            this.DeviceCount.Location = new System.Drawing.Point(280, 14);
            this.DeviceCount.Name = "DeviceCount";
            this.DeviceCount.Size = new System.Drawing.Size(61, 20);
            this.DeviceCount.TabIndex = 8;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(194, 17);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(86, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Unique Devices:";
            // 
            // OldestUUID
            // 
            this.OldestUUID.Location = new System.Drawing.Point(88, 72);
            this.OldestUUID.Name = "OldestUUID";
            this.OldestUUID.Size = new System.Drawing.Size(293, 20);
            this.OldestUUID.TabIndex = 10;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(42, 75);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(40, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Oldest:";
            // 
            // GeoCount
            // 
            this.GeoCount.Location = new System.Drawing.Point(280, 40);
            this.GeoCount.Name = "GeoCount";
            this.GeoCount.Size = new System.Drawing.Size(61, 20);
            this.GeoCount.TabIndex = 12;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(216, 43);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(61, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "Geo Count:";
            // 
            // NewestUUID
            // 
            this.NewestUUID.Location = new System.Drawing.Point(87, 98);
            this.NewestUUID.Name = "NewestUUID";
            this.NewestUUID.Size = new System.Drawing.Size(294, 20);
            this.NewestUUID.TabIndex = 14;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(38, 101);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(46, 13);
            this.label6.TabIndex = 13;
            this.label6.Text = "Newest:";
            // 
            // OldestDate
            // 
            this.OldestDate.Location = new System.Drawing.Point(387, 72);
            this.OldestDate.Name = "OldestDate";
            this.OldestDate.Size = new System.Drawing.Size(205, 20);
            this.OldestDate.TabIndex = 15;
            // 
            // NewestDate
            // 
            this.NewestDate.Location = new System.Drawing.Point(387, 98);
            this.NewestDate.Name = "NewestDate";
            this.NewestDate.Size = new System.Drawing.Size(205, 20);
            this.NewestDate.TabIndex = 16;
            // 
            // main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(604, 167);
            this.Controls.Add(this.NewestDate);
            this.Controls.Add(this.OldestDate);
            this.Controls.Add(this.NewestUUID);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.GeoCount);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.OldestUUID);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.DeviceCount);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.TotalCount);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.UniqueCount);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ProcessData);
            this.Name = "main";
            this.Text = "Southern Cross Engineering Challenge";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button ProcessData;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox UniqueCount;
        private System.Windows.Forms.TextBox TotalCount;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox DeviceCount;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox OldestUUID;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox GeoCount;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox NewestUUID;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox OldestDate;
        private System.Windows.Forms.TextBox NewestDate;
    }
}

