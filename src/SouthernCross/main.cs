﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;


namespace SouthernCross
{
    public partial class main : Form
    {
        public main()
        {
            InitializeComponent();
        }

        /// <summary>
        /// ProcessData_Click: Main processing workflow that steps 
        /// through the required processes for the Challenge
        /// </summary>
        /// <param name="sender">UI button the launches the method</param>
        /// <param name="e">Parameters associated with the button click event</param>
        private void ProcessData_Click(object sender, EventArgs e)
        {
            // Setup a list to hold all device records 
            // supplied in the data files
            //------------------------------------------
            List<Device> devices = new List<Device>();

            // Ingest data files
            //-----------------------------
            try
            {
                ReadCSV(@"main\resources\data.csv", devices);
                ReadJSON(@"main\resources\data.json", devices);
            }
            catch (Exception ex)
            {
                // Let the user know about any errors that occur
                //------------------------------------------------
                MessageBox.Show(ex.Message);
            }

            // Save the initial record count in a UI text field
            //----------------------------------------------------
            TotalCount.Text = devices.Count.ToString();

            // Save the unique counts (unique records and unique
            // devices, based on UUID for each record)
            //----------------------------------------------------
            UniqueCount.Text = devices.Select(x => new { x.uuid, x.latitude, x.longitude, x.timestamp }).Distinct().Count().ToString();
            DeviceCount.Text = devices.Select(x => x.uuid).Distinct().Count().ToString();

            // Determine the number of devices within specified geo-range
            //-------------------------------------------------------------
            GeoCount.Text = devices.Where(x => x.latitude >= 0 && x.latitude < 90
                         && x.longitude <= 0 && x.longitude > -180).Count().ToString();

            // Determine Oldest and Newest timestamp
            //-----------------------------------------
            devices = devices.OrderBy(x => x.timestamp).ToList();
            DateTime startDate = new DateTime(1970, 1, 1);
            DateTime date = startDate.AddSeconds(devices[0].timestamp);
            OldestUUID.Text = devices[0].uuid;
            OldestDate.Text = date.ToString();

            date = startDate.AddSeconds(devices[devices.Count-1].timestamp);
            NewestUUID.Text = devices[devices.Count - 1].uuid;
            NewestDate.Text = date.ToString();

            //====================================================
            // Report the results
            //====================================================
            // Create the dictionaries that will be nested later
            //----------------------------------------------------
            OrderedDictionary results = new OrderedDictionary();
            OrderedDictionary oldest = new OrderedDictionary();
            OrderedDictionary newest = new OrderedDictionary();

            // Add the simple name/value pairs first
            //---------------------------------------
            results.Add("total_count", TotalCount.Text);
            results.Add("unique_device_count", DeviceCount.Text);
            results.Add("geo_range_count", GeoCount.Text);

            // Create a nested dictionary for the oldest record
            // and add it to the results (parent) dictionary
            //---------------------------------------------------
            oldest.Add("uuid", OldestUUID.Text);
            oldest.Add("timestamp", OldestDate.Text);
            results.Add("oldest", oldest);

            // Create a nested dictionary for the newest record
            // and add it to the results (parent) dictionary
            //---------------------------------------------------
            newest.Add("uuid", NewestUUID.Text);
            newest.Add("timestamp", NewestDate.Text);
            results.Add("newest", newest);

            // Finally, build a JSON string representation of
            // the nested results dictionary, and write that
            // string to the specified file
            //--------------------------------------------------
            File.WriteAllText(@"main\resources\results.json", BuildJSONString("results", results, 0));
            //====================================================
            // End of Results Reporting
            //====================================================
        }

        /// <summary>
        /// ReadCSV: Read a comma separated values file
        /// </summary>
        /// <param name="file">Path to the CSV file</param>
        /// <param name="devices">List of type Device</param>
        private void ReadCSV(String file, List<Device> devices) 
        {
            String line = "";
            string[] values;
            double d;

            // Open the specified file
            //---------------------------------
            StreamReader reader = new StreamReader(file);

            // Read and process each line, one at a time
            //---------------------------------------------
            try
            {
                do
                {
                    line = reader.ReadLine();
                    values = line.Split(',');

                    // If the latitude field is NOT numeric, skip
                    // it because this is a header row.
                    //---------------------------------------------
                    if (double.TryParse(values[1], out d))
                    {
                        // Add each device to the devices list
                        //-----------------------------------------
                        devices.Add(new Device(values[0], Convert.ToDouble(values[1]), Convert.ToDouble(values[2]), Convert.ToInt64(values[3])));
                    }
                }
                // Repeat until end of file is reached
                //--------------------------------------
                while (reader.Peek() != -1);
            }
            catch
            {
                // Pass the exception up to the calling routine
                //----------------------------------------------
                throw;
            }

            finally
            {
                // Close the file connection before exiting
                //-------------------------------------------
                reader.Close();
            }
        }

        /// <summary>
        /// ReadJSON: Reads a JSON file in the specified format.
        /// </summary>
        /// <param name="file">Path the JSON file</param>
        /// <param name="devices">List of type Device</param>
        private void ReadJSON(String file, List<Device> devices)
        {
            // No need to add an entire framework just to read a
            // file with a known format.  So just fetch the data
            // using simple read operations
            //---------------------------------------------------
            String line = "";
            String uuid = "";
            String lat = "";
            String lng = "";
            String time = "";

            // Open the specified file
            //---------------------------------
            StreamReader reader = new StreamReader(file);

            // Process the file one line at a time using
            // key-value pair spotting
            //---------------------------------------------
            try
            {
                do
                {
                    line = reader.ReadLine();

                    // Store the key-value pairs as they are encountered
                    //------------------------------------------------------
                    if (line.Contains("\"uuid\"")) uuid = GetValue(line);
                    if (line.Contains("\"lat\"")) lat = GetValue(line);
                    if (line.Contains("\"long\"")) lng = GetValue(line);
                    if (line.Contains("\"timestamp\"")) time = GetValue(line);

                    // If the last field (time) has been populated
                    // add the whole device to the list o devices
                    //-----------------------------------------------------
                    if (time != "")
                    {
                        // Make sure all the data is there.  If not, throw and error
                        //-----------------------------------------------------------
                        if (uuid == "" || lat == "" || lng == "")
                        {
                            throw new System.ArgumentException("Value may not be empty.");
                        } 

                        // Add each device to the devices list
                        //-----------------------------------------
                        devices.Add(new Device(uuid, Convert.ToDouble(lat), Convert.ToDouble(lng), Convert.ToInt64(time)));
                        
                        // Clear the fields before starting on a new device
                        //--------------------------------------------------
                        uuid = "";
                        lat = "";
                        lng = "";
                        time = "";
                    }
                }
                // Repeat until end of file is reached
                //--------------------------------------
                while (reader.Peek() != -1);
            }
            catch
            {
                // Pass the exception up to the calling routine
                //----------------------------------------------
                throw;
            }

        }

        /// <summary>
        /// GetValue: Splits an input line into separate parts and
        /// trim off unwanted characters from the specified JSON value
        /// </summary>
        /// <param name="line">An input string representing one line 
        /// of data read form a JSON file</param>
        /// <returns>The trimmed value from a name/value pair</returns>
        private string GetValue(String line)
        {
            // Split the parameter name from its value
            //------------------------------------------
            String[] parts = line.Split(':');
            String value = parts[1].Trim();

            // Remove unwanted characters from the start and end of the value
            //-----------------------------------------------------------------
            char[] charsToTrim = { '\"', ' ', '\'', ','};
            value = value.Trim(charsToTrim);

            // Return the value to the calling method
            //-----------------------------------------
            return value;
        }

        /// <summary>
        /// BuildJSONString: Recursive method that builds JSON strings
        /// from nested Ordered Dictionaries of name/value pairs
        /// </summary>
        /// <param name="root">Name of the parent JSON object that 
        /// contains the name/values pairs in the Ordered Dictionary</param>
        /// <param name="values">Ordered Disctionary of name/value
        /// pairs.  Values may be strings or Ordered Dictionaries</param>
        /// <param name="level">Indentation level, starting at zero</param>
        /// <returns>String representation of the JSON object</returns>
        private String BuildJSONString(String root, OrderedDictionary values, int level)
        {
            String output = "";
            string spacer = "";

            // Setup a spacer with 2 spaces for each level above the first
            //-------------------------------------------------------------
            for (int i = 0; i < level; i++) spacer += "  ";

            // Create the (possibly nested) parent element
            //-----------------------------------------------
            if (level == 0) output += "{";
            output += spacer + "\"" + root + "\": \r\n" + spacer + "  {\r\n";

            // Process each name/value pair, one at a time
            //-----------------------------------------------
            foreach (String name in values.Keys)
            {
                // If the current value is a nested Ordered Disctionary,
                // call this same method recursively, otherwise just add
                // the name/value pair to the string
                //--------------------------------------------------------
                if (values[name].GetType() == typeof(OrderedDictionary))
                    output += "  " + BuildJSONString(name, (OrderedDictionary)values[name], level+1);
                else
                    output += spacer + "    \"" + name + "\": \"" + values[name] + "\"";

                // End each pair with a comma, unless it's the last name/value pair
                //---------------------------------------------------------------------
                if (values[name].Equals(values[values.Count - 1]))
                    output += "\r\n";
                else
                    output += ",\r\n";
            }

            // Close out the current parent element
            //---------------------------------------
            output += spacer + "  }";

            // If this is the root element, close out the whole JSON object
            //---------------------------------------------------------------
            if (level == 0) output += "\r\n}";

            // Return the JSON string (or substring)
            //---------------------------------------
            return output;
        }
    }
}
